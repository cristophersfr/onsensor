import { Injectable } from '@angular/core';

import { GeneticAlgorithm } from './genetic_algorithm';
import { Individual } from './individual';

import { Scenario } from '../wsn/scenario/scenario';
import { Vehicle } from '../wsn/vehicle/vehicle';
import { RSU } from '../wsn/rsu/rsu';

@Injectable()
export class GaService {
  private geneticAlgorithm: GeneticAlgorithm;
  constructor() {}

  create(): GeneticAlgorithm {
    return this.geneticAlgorithm = new GeneticAlgorithm(100, 100, 0.95, 0.05);
  }

  init(scenario: Scenario, vehicles: Vehicle[], rsus: RSU[]): void {
    this.geneticAlgorithm.initialize(scenario, vehicles, rsus);
  }

  update(geneticAlgorithm: GeneticAlgorithm): void {
    this.geneticAlgorithm = geneticAlgorithm;
  }

  run(): void {
    this.geneticAlgorithm.run();
  }

  getFittest(): Individual {
    return this.geneticAlgorithm.getFittest();
  }

}
