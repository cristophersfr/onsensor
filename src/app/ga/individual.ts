/**
 * Individual of Genetic Algorithm
 */

export class Individual {
    genome: {x: number, y: number}[] = [];
    fitness: number;
}
