import { Component, OnInit, OnChanges } from '@angular/core';

import { GeneticAlgorithm } from './genetic_algorithm';
import { Individual } from './individual';
import { Scenario } from '../wsn/scenario/scenario';
import { Vehicle } from '../wsn/vehicle/vehicle';
import { RSU } from '../wsn/rsu/rsu';

import { GaService } from './ga.service';
import { ScenarioService } from '../wsn/scenario/scenario.service';
import { RsuService } from '../wsn/rsu/rsu.service';
import { VehicleService } from '../wsn/vehicle/vehicle.service';

@Component({
  selector: 'app-ga',
  templateUrl: './ga.component.html',
  styleUrls: ['./ga.component.css'],
  providers: [GaService, ScenarioService, RsuService, VehicleService]
})

export class GaComponent implements OnInit, OnChanges {
  geneticAlgorithm: GeneticAlgorithm;
  scenario: Scenario;
  rsus: RSU[];
  vehicles: Vehicle[];

  constructor(private gaService: GaService, private scenarioService: ScenarioService,
    private vehicleService: VehicleService, private rsuService: RsuService) {
    this.geneticAlgorithm = this.gaService.create();
    this.scenario = this.scenarioService.getScenario();
    this.rsus = this.rsuService.getRSUs();
    this.vehicles = this.vehicleService.getVehicles();
  }

  ngOnChanges() {
    this.gaService.update(this.geneticAlgorithm);
  }

  ngOnInit() {
    this.gaService.init(this.scenario, this.vehicles, this.rsus);
  }

  save(): void {
    this.gaService.run();
  }

}
