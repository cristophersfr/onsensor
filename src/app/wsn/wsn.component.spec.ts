import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WsnComponent } from './wsn.component';

describe('WsnComponent', () => {
  let component: WsnComponent;
  let fixture: ComponentFixture<WsnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WsnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WsnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
