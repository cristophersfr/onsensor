import { TestBed, inject } from '@angular/core/testing';

import { RsuService } from './rsu.service';

describe('RsuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RsuService]
    });
  });

  it('should be created', inject([RsuService], (service: RsuService) => {
    expect(service).toBeTruthy();
  }));
});
