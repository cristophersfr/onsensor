import { Scenario } from './wsn/scenario/scenario';
import { Vehicle } from './wsn/vehicle/vehicle';
import { RSU } from './wsn/rsu/rsu';

export const SCENARIO: Scenario = {
    size: {x: 500, y: 500}
};
